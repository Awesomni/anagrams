"""Модуль для нахождения анаграмм."""
import sys
from collections import Counter


def load_dict(file_path):
    """Открывает файл и возвращает множество (set) строк в нижнем регистре."""
    try:
        with open(file_path) as in_file:
            print(f'Загружено {len(in_file.readlines())} записей\n')
            return {x.lower() for x in in_file.read().strip().split('\n')}
    except IOError as err:
        print(f'{err}\nОшибка при открытии {file_path}. Программа завершается', file=sys.stderr)


def is_anagram(_a: str, _b: str) -> bool:
    """Проверяет являются ли две строки анаграммами."""
    return sorted(_a) == sorted(_b)


def find_anagrams_for_word(word: str) -> list:
    """Отыскивает анаграммы в словаре."""
    return [w for w in load_dict('/usr/share/dict/web2') if is_anagram(w, word)]


def cv_word(word: str) -> str:
    vowels = 'aeyuioAEYUIO'
    cv = ''
    for letter in word:
        if letter in vowels:
            cv += 'v'
        else:
            cv += 'c'
    return cv


def get_cv_set_from_real_dict() -> set:
    words = load_dict('/usr/share/dict/web2')
    cv_map = []

    for word in words:
        print(word)
        cv_map.append(cv_word(word))
    return set(cv_map)


def get_anagram_names(name: str):
    from itertools import permutations
    perms = [''.join(i) for i in permutations(name)]
    cvs = get_cv_set_from_real_dict()
    filtered_cv = [i for i in perms if cv_word(i) in cvs]
    return filtered_cv


def main():
    """Главная функция."""
    while True:
        ask = input('Введите имя для составления анаграммы: ')
        if ask == '':
            print('\n\nСпасибо, что пользуетесь моей программой.\nВсего хорошего!')
            sys.exit(1)
        print(f'Ищем анаграммы к слову {ask}')
        anas = get_cv_set_from_real_dict() # get_anagram_names(ask)
        if len(anas) < 1:
            print('Ничего не найдено, попробуйте ещё :-)')
        else:
            for idx, val in enumerate(anas, 1):
                print(f'{idx:<5}{val:>10}')


if __name__ == '__main__':
    main()
